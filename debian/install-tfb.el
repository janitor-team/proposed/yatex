(defun bcf-and-exit ()
  "Byte compile rest of argument and kill-emacs."
  (if command-line-args-left
      (let ((load-path (cons "." load-path)))
	(and (fboundp 'set-language-environment)
	     (featurep 'mule)
	     (set-language-environment "Japanese"))
	(mapcar 'byte-compile-file command-line-args-left)
	(kill-emacs))))

(defun tfb-and-exit ()
  "Texinfo-format-buffer and kill-emacs."
  (if command-line-args-left
      (let ((load-path (cons ".." load-path)))
	(and (fboundp 'set-language-environment)
	     (featurep 'mule)
	     (set-language-environment "Japanese"))
	(mapcar (function
		 (lambda (arg)
		   (find-file arg)
		   (texinfo-format-buffer)
		   (cond
		    ((fboundp 'set-buffer-file-coding-system)
		     (set-buffer-file-coding-system 'utf-8-unix))
		    ((fboundp 'set-file-coding-system)
		     (set-file-coding-system '*utf-8*unix))
		    ((boundp 'NEMACS)
		     (set (make-local-variable 'kanji-fileio-code) 1)))
		   (let ((coding-system-for-write buffer-file-coding-system))
		     (basic-save-buffer))))
		command-line-args-left)
	(kill-emacs))))

